import re
#from collections import defaultdict
# from heatDiffusion import *
import numpy as np
from scipy.linalg import expm
import random
import math

##friendsDictionary: Dictionary for connectivity. Nodes and their neighbours
friendsDictionary = {}

##media Dictionary of each author
nonInterActiveDict = {}

##User Dictionary of each follower (source node)
interActiveDict = {}

##Dictionary for mapping Original Node ID to the new Node ID for the purpose of putting in a matrix form [Kernel]
nodesMappingDict = {}

##Heat Distribution Matrix: contains the heat at each node at time t , H(t)
#heatDistributionMatrix = []

def fileRead(fileName):
    inputFile = open(fileName, 'r')
    next(inputFile)  ##skip column header

    global friendsDictionary    #friendsDictionary: Dictionary containing the author ID and its neighbours list
    global nonInterActiveDict   #nonInterActiveDict: Dictionary containing the author ID and the no. of media posted by the author.
    global interActiveDict      #interActiveDict: Dictionary containing key-value pair of author ID and Dictionary containing neighbourID and the associated likes and comments
    global nodesMappingDict     #nodesMappingDict: Dictionary containing key-value pair of author ID and new serial authorID

    ###for media file
    if 'media' in fileName:

        # mediaAuthorList = []
        mediaDict = {}

        for eachLine in iter(inputFile):
            eachLine = eachLine.rstrip('\r\n')

            lineList = eachLine.split(';')
            try:

                ###for instagram media data set (mediaID, authorID)
                mediaID = int(lineList[0])
                authorID = int(lineList[1])
                # mediaTuple = (authorID, mediaID)
                # mediaAuthorList.append(mediaTuple)

                # if mediaDict.has_key(authorID):
                #     mediaDict[authorID].append(mediaID)
                # else:
                #     mediaDict[authorID] = [(mediaID)]

                # if mediaDict.has_key(authorID):
                #     nonInterActiveDict[authorID] += 1
                # else:
                #     nonInterActiveDict[authorID] = 1

                # for item in lineList:
                #     if re.match('^\d+$', item):
                #        intList.append(int(item))

                # mediaDict = defaultdict(list)

                # for authorID, mediaID in mediaAuthorList:
                #     mediaDict[authorID].append(mediaID)

                ###mediaDict: Dictionary containing the author ID and list of medias

                if not authorID in mediaDict:
                    mediaDict[authorID] = [mediaID]
                else:
                    mediaDict[authorID].append(mediaID)
            except ValueError:
                pass

        ###nonInterActiveDict: Dictionary containing the author ID and the no. of media posted by the author.
        for authorID in mediaDict:
            noOfMedia = len(mediaDict[authorID])
            # print authorID, noOfMedia
            nonInterActiveDict[authorID] = noOfMedia
            friendsDictionary[authorID] = []   #authorID as source node and target nodes -> empty neighbors' list

    ###for User file
    if 'user' in fileName:

        for eachLine in iter(inputFile):
            lineList = eachLine.split(';')

            ###for instagram user data set (follower, followee, No. of likes, No. of comments)
            followerID = int(lineList[0])
            followeeID = int(lineList[1])
            likes = int(lineList[2])
            comments = int(lineList[3])

            if not followerID in interActiveDict:
                followeeDict = {}    #Followee Dictionary
                # followeeDict[followeeID] = (likes, comments)
                followeeDict[followeeID] = likes + comments #Total no. of interactive activities

                interActiveDict[followerID] = followeeDict
            else:
                #inserting key-value pair inside the value(which is in the form dictionary) of interActiveDict
                # interActiveDict[followerID][followeeID] = (likes, comments)
                interActiveDict[followerID][followeeID] = likes + comments

            #making a list of every node and its neighbors (followerID -> followeeID)
            if not followerID in friendsDictionary:
                friendsDictionary[followerID] = [followeeID]
            else:
                friendsDictionary[followerID].append(followeeID)

            #making a list of every node (followeeID)
            if not followeeID in friendsDictionary:
                friendsDictionary[followeeID] = []


        # for eachTuple in userList:
        #     followerDict[eachTuple[1]] = (eachTuple[2], eachTuple[3])   #followerDict[followeeID] = (likes, comments)
        #     interActiveDict[eachTuple[0]] = followerDict                #interActiveDict[followerID] = followerDict

    newFolloweeID = 0
    for followeeID in friendsDictionary:
        nodesMappingDict[followeeID] = newFolloweeID
        newFolloweeID = newFolloweeID + 1

    inputFile.close()

def splitList(lineList):
    intList = []
    for item in lineList:
        if re.match('^\d+$', item):
           intList.append(int(item))
        # else:
        #    wordList.append(item)
    # return intList, wordList
    return intList


#def calculateDiffusionKernel(weight_na, weight_ia):
def calculateHeatDistribution(thermalConduct, weight_na, weight_ia, initialHeat):
    #calculateDiffusionKernel
    noOfNodes = len(friendsDictionary)
    diffKernelMatrix = np.zeros((noOfNodes, noOfNodes))

    for followerID in friendsDictionary:
        newFollowerID = nodesMappingDict.get(followerID)

        #To compute K(i,j) when (v_j,v_i) belongs to E
        followeeList = friendsDictionary.get(followerID)
        for followeeID in followeeList:
            newFolloweeID = nodesMappingDict.get(followeeID)
            diffKernelMatrix[newFollowerID][newFolloweeID] = calcHeatReceived(followerID, followeeID)

        #K(i,j) when i = j and d_i > 0
        if len(friendsDictionary.get(followerID)) is not 0:
            NA_follower = nonInterActiveDict.get(followerID)
            IATotal_follower = findTotalInterActivities(followerID)
            # IATotal_follower = diffKernelMatrix[newFollowerID][newFollowerID] = -1

            # if (NA_follower is not 0 and math.isnan(NA_follower) is False) and IATotal_follower is not 0:
            if (NA_follower is not 0 and NA_follower is not None) and IATotal_follower is not 0:

                # diffKernelMatrix[newFollowerID][newFollowerID] = 1.0 - ((NA_follower*weight_na)/((NA_follower*weight_na)+(weight_ia*IATotal_follower)))
                numerator = NA_follower*weight_na
                denominator = (NA_follower*weight_na)+(weight_ia*IATotal_follower)
                if denominator is not 0:
                    diffKernelMatrix[newFollowerID][newFollowerID] = 1.0 - (numerator /denominator)

    #calculate Heat Difusion at Time t, H(t)
    H_0 = np.full((noOfNodes,1), initialHeat)
    H_t = thermalConduct * diffKernelMatrix   #alpha*t*K  -> we take t=1
    global heatDistributionMatrix
    heatDistributionMatrix = expm(H_t) * H_0
    print "heatDistributionMatrix : \n", heatDistributionMatrix
    #view matrix value: m.item(0,1) or m[0,1]
    #setting a new value: a[i,j] = x or a.itemset((i,j),x)

def calcHeatReceived(node1, node2):
    IA_n1_n2 = findInterActivities(node1, node2)   #IA_n1_to_n2 + IA_n2_to_n1
    IATotal_n2 = findTotalInterActivities(node2)
    print "IATotal_n2: %d" % IATotal_n2
    print "IA_n1_n2: %d" % IA_n1_n2
    # if math.isnan(IATotal_n2):
    #     return 0
    # else:
    #     return IA_n1_n2/IATotal_n2
    if (IA_n1_n2 is 0 or IA_n1_n2 is None) or (IATotal_n2 is 0 or IATotal_n2 is None):
        return 0
    else:
        return IA_n1_n2/IATotal_n2


def findInterActivities(node1, node2):
    #get the IA value of neighbour n2 of node n1 and vice-versa
    #n1->n2 and n2->n1
    # return interActiveDict[node1].get(node2) + interActiveDict[node2].get(node1)
    print "node1: %d" % node1
    print "node2: %d" % node2
    # print interActiveDict[node1][node2]
    # print interActiveDict[node2][node1]
    print interActiveDict.get(node2)
    if node1 in interActiveDict and node2 in interActiveDict.get(node1):
        interActiveDict_1_2 = interActiveDict[node1][node2]
    else:
        interActiveDict_1_2 = 0

    if node2 in interActiveDict and node1 in interActiveDict.get(node2):
        interActiveDict_2_1 = interActiveDict[node2][node1]
    else:
        interActiveDict_2_1 = 0
    return interActiveDict_1_2 + interActiveDict_2_1

def findTotalInterActivities(node):
    neighboursList = friendsDictionary.get(node)
    IATotal_n = 0        #sum of all the non-interactive activities, a node has with its neighbours

    for neighbour_t in neighboursList:
        IA_nt = findInterActivities(node, neighbour_t)
        IATotal_n += IA_nt

    return IATotal_n

def inputCheck(userValue):
    try:
        int(userValue)
        return 0
    except ValueError:
        try:
            float(userValue)
            return 0
        except ValueError:
            print "This is not a number"
            print "Default value will be used"
            return 1



def getParameterInput():
    thermalConduct = raw_input("Provide the thermal conductivity: ") #heat conductivity - alpha
    checkResult = inputCheck(thermalConduct)
    if checkResult is 1:
        thermalConduct = random.random()  #a random floating point number in the range [0, 1)
    else:
        thermalConduct = float(thermalConduct)

    weight_na = raw_input("Provide the weightage for Non-Interactive Activities [between 0 to 1]: ")
    checkResult = inputCheck(weight_na)
    if checkResult is 1:
        weight_na = 0.4
    else:
        weight_na = float(weight_na)

    weight_ia = raw_input("Provide the weightage for Interactive Activities [between 0 to 1]: ")
    checkResult = inputCheck(weight_ia)
    if checkResult is 1:
        weight_ia = 0.6   #weight_na = 40% & weight_ia = 60% -interactive activities given more weightage
    else:
        weight_ia = float(weight_ia)

    initialHeat = raw_input("Provide the the Initial Heat for diffusion: ")
    checkResult = inputCheck(initialHeat)
    if checkResult is 1:
        initialHeat = 30.0   #the same value as given in Doo et al. paper for reference
    else:
        initialHeat = float(initialHeat)

    return thermalConduct, weight_na, weight_ia, initialHeat

def main():
    # global mediaDict = {}
    fileName = 'media_test.csv'
    fileRead(fileName)

    fileName = 'users_test.csv'
    fileRead(fileName)

    print "friendsDictionary -> ", friendsDictionary
    print "interActiveDict with each Neighbors ->", interActiveDict
    print "nonInterActiveDict ->",nonInterActiveDict

    thermalConduct, weight_na, weight_ia, initialHeat = getParameterInput()
    print thermalConduct, weight_na, weight_ia, initialHeat

    #calculateDiffusionKernel(weight_na, weight_ia)
    calculateHeatDistribution(thermalConduct, weight_na, weight_ia, initialHeat)


if __name__ == "__main__":
    # import sys
    # print "test"
    # sys.exit(1)
    main()
